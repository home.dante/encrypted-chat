const jwt = require("jsonwebtoken");
const AccessControl = require("accesscontrol");
const ac = new AccessControl();
const mongoose = require("mongoose");
//customer access rights
ac.grant("customer")
  .readOwn("users")
  .updateOwn("users")
  .createAny("orders")
  .readOwn("orders")
  .readAny("products") //not needed, /products/ not protected by this middleware
  //vendor access rights
  .grant("vendor")
  .readOwn("users")
  .updateOwn("users")
  .readAny("products") //not needed, /products/ not protected by this middleware
  .updateOwn("products")
  .createAny("products")
  .deleteOwn("products")
  //admin access rights
  .grant("admin")
  .extend(["customer", "vendor"])
  .readAny("users")
  .createAny("users")
  .updateAny("users")
  .deleteAny("users")
  .createAny("products")
  .updateAny("products")
  .deleteAny("products")
  .createAny("orders")
  .updateAny("orders")
  .deleteAny("orders");

module.exports = (req, res, next) => {
  const resourceID = req.params.ID;
  const token = req.headers.authorization.split(" ")[1];
  const decoded = jwt.verify(token, process.env.JWT_KEY);
  const userID = decoded.userID;
  const role = decoded.userRole;
  const resource = req.baseUrl.split("/")[1];
  const method = req.method;
  console.log("resource:", resource);
  console.log("role", role);
  if (method === "GET") {
    if (ac.can(role).readAny(resource).granted) {
      next();
    } else if (ac.can(role).readOwn(resource).granted) {
      console.log("res id: ", resourceID);
      mongoose
        .model(getModelName(resource))
        .findById(resourceID)
        .exec()
        .then((response) => {
          console.log("RESOURCE IS: ", res);
          if (!response) {
            res.status(404).json({ message: "Resource not found" });
          }
          if (response.owner == userID) {
            console.log("PERMISSION GRANTED");
            next();
          } else {
            console.log("PERMISSION DENIED");
            res.status(403).json({ message: "Forbidden Access" });
          }
        })
        .catch((err) => {
          console.log(err);
          console.log("PERMISSION DENIED");
          res.status(403).json({ message: "Forbidden Access" });
        });
    } else {
      res.status(403).json({ message: "Forbidden Access" });
    }
  } else if (method === "POST") {
    if (ac.can(role).createAny(resource).granted) {
      next();
    } else if (ac.can(role).createOwn(resource).granted) {
      console.log("res id: ", resourceID);
      mongoose
        .model(getModelName(resource))
        .findById(resourceID)
        .exec()
        .then((res) => {
          if (res.owner && res.owner == userID) {
            console.log("PERMISSION GRANTED");
            next();
          } else {
            console.log("PERMISSION DENIED");
            res.status(403).json({ message: "Forbidden Access" });
          }
        })
        .catch((err) => {
          console.log(err);
          console.log("PERMISSION DENIED");
          res.status(403).json({ message: "Forbidden Access" });
        });
    } else {
      res.status(403).json({ message: "Forbidden Access" });
    }
  } else if (method === "DELETE") {
    if (ac.can(role).deleteAny(resource).granted) {
      next();
    } else if (ac.can(role).deleteOwn(resource).granted) {
      console.log("res id: ", resourceID);
      mongoose
        .model(getModelName(resource))
        .findById(resourceID)
        .exec()
        .then((response) => {
          if (!response) {
            res.status(404).json({ message: "Resource not found" });
          }
          if (response.owner == userID) {
            console.log("PERMISSION GRANTED");
            next();
          } else {
            console.log("PERMISSION DENIED");
            res.status(403).json({ message: "Forbidden Access" });
          }
        })
        .catch((err) => {
          console.log(err);
          console.log("PERMISSION DENIED");
          res.status(403).json({ message: "Forbidden Access" });
        });
    } else {
      res.status(403).json({ message: "Forbidden Access" });
    }
  } else if (method === "PATCH") {
    if (ac.can(role).updateAny(resource).granted) {
      next();
    } else if (ac.can(role).updateOwn(resource).granted) {
      console.log("res id: ", resourceID);
      mongoose
        .model(getModelName(resource))
        .findById(resourceID)
        .exec()
        .then((res) => {
          if (res.owner && res.owner == userID) {
            console.log("PERMISSION GRANTED");
            next();
          } else {
            console.log("PERMISSION DENIED");
            res.status(403).json({ message: "Forbidden Access" });
          }
        })
        .catch((err) => {
          console.log(err);
          console.log("PERMISSION DENIED");
          res.status(403).json({ message: "Forbidden Access" });
        });
    } else {
      res.status(403).json({ message: "Forbidden Access" });
    }
  } else if (method === "PUT" && ac.can(role).createAny(resource).granted) {
    next();
  } else {
    res.status(403).json({ message: "Forbidden Access" });
  }
};
function getModelName(resource) {
  switch (resource) {
    case "products":
      return "Product";
      break;
    case "users":
      return "User";
      break;
    case "orders":
      return "Order";
      break;
    default:
      return "Product";
  }
}
