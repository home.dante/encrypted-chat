const mongoose = require("mongoose");

const productSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: { type: String, unique: true, required: true, dropDups: true },
  price: { type: Number, required: true },
  productImage: { type: String, required: true },
  productImageAbsolute: { type: String, required: true },
  // productGallery: [{ url: String }],
  type: {
    type: String,
    enum: ["pizza", "sandwich", "beverage"],
    required: true
  },
  sizes: [
    {
      price: { type: Number },
      enabled: { type: Boolean },
      size: {
        type: String,
        enum: ["23cm", "30cm", "40cm", "50cm", "60cm"]
      }
    }
  ],
  ingredients: [{ name: String, value: String }],
  added: { type: Number }
  // vendor: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
  // owner: { type: mongoose.Schema.Types.ObjectId, required: true }
});
// productSchema.index({ name: 1 }, (err, result) => {
//   console.log(result);
//   // cb(result);
// });
productSchema.pre("save", function(next) {
  // orderSchema.owner = orderSchema._id;

  this.added = new Date().getTime();
  next();
});
module.exports = mongoose.model("Product", productSchema);
