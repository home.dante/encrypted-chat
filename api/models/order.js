const mongoose = require("mongoose");

const orderSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  products: [
    {
      product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product",
        required: true
      },
      productQuantity: { type: Number, default: 1 },
      size: { size: { type: String }, price: { type: Number } },
      customIngredients: [{ type: String }]
    }
  ],
  total: {
    type: Number
  },
  billing: {
    name: { type: String, required: true },
    address: { type: String, required: true },
    phone: { type: Number, required: true },
    email: { type: String, required: true }
  }

  // user: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
  // owner: { type: mongoose.Schema.Types.ObjectId, required: true }
});
orderSchema.pre("save", function(next) {
  // orderSchema.owner = orderSchema._id;

  // this.total = 150;
  next();
});

module.exports = mongoose.model("Order", orderSchema);
