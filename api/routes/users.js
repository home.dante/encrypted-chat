const express = require("express");
const router = express.Router();
// const checkAuth = require("../middleware/check-auth");
const authorize = require("../middleware/authorize2.0");

const UsersController = require("../controllers/users");

// //CREATE USER
// router.put("/signup", UsersController.users_create);

// //GET USER BY ID
// router.get("/:ID", checkAuth, authorize, UsersController.users_get_one_by_ID);

// //UPDATE USER BY ID
// router.patch("/:ID", checkAuth, authorize, UsersController.users_update);

// //DELETE USER
// router.delete("/:ID", checkAuth, authorize, UsersController.users_delete);

// //LOGIN USER
// router.post("/login", UsersController.users_login);

// //GET ALL USERS
// router.get("/", checkAuth, authorize, UsersController.users_get_all);

module.exports = router;
