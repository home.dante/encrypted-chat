const express = require("express");
const router = express.Router();
const authorize = require("../middleware/authorize2.0");

const OrdersController = require("../controllers/orders");
// const checkAuth = require("../middleware/check-auth");

//FETCH ALL ORDERS
router.get("/", OrdersController.orders_get_all);

//CREATE ORDER
router.put("/", OrdersController.orders_create);

//GET ORDER BY ID
router.get("/:ID", authorize, OrdersController.orders_get_one_by_id);

//DELETE ORDER
router.delete(
  "/:ID",

  authorize,
  OrdersController.orders_delete_by_id
);

module.exports = router;
