const mongoose = require("mongoose");
// const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const serverUrl = process.env.SERVER_URL + "users/";
const User = require("../models/user");

exports.users_get_all = (req, res, next) => {
  User.find()
    .populate("products ", "-__v")
    .select("-__v -role")
    .exec()
    .then((docs) => {
      const response = {
        request: {
          type: "GET",
          description: "Get all Users",
          url: serverUrl
        },
        count: docs.length,
        users: docs.map((doc) => {
          return {
            email: doc.email,
            _id: doc._id,
            products: doc.products,
            request: {
              type: "GET",
              url: serverUrl + doc._id,
              protected: true
            }
          };
        })
      };
      if (docs.length > 0) {
        res.status(200).json(response);
      } else {
        res.status(200).json({ message: "No entries found" });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.users_login = (req, res, next) => {
  // User.find({ email: req.body.email }, "-__v")
  //   .exec()
  //   .then((user) => {
  //     if (user.length < 1) {
  //       return res.status(401).json({ message: "Auth failed" });
  //     } else {
  //       bcrypt.compare(req.body.password, user[0].password, (err, result) => {
  //         if (err) {
  //           return res.status(401).json({
  //             message: "Auth failed"
  //           });
  //         }
  //         if (result) {
  //           const token = jwt.sign(
  //             {
  //               email: user[0].email,
  //               userID: user[0]._id,
  //               userRole: user[0].role
  //             },
  //             process.env.JWT_KEY,
  //             {
  //               expiresIn: "100h"
  //             }
  //           );
  //           return res.status(200).json({
  //             message: "Auth Successfull",
  //             token: token,
  //             user: {
  //               email: user[0].email,
  //               id: user[0]._id
  //             },
  //             request: {
  //               type: "POST",
  //               url: serverUrl + "login"
  //             }
  //           });
  //         }
  //         return res.status(401).json({
  //           message: "Auth failed"
  //         });
  //       });
  //     }
  //   })
  //   .catch((err) => {
  //     console.log(err);
  //     res.status(500).json({
  //       error: err,
  //       request: {
  //         type: "POST"
  //       }
  //     });
  //   });
};

exports.users_delete = (req, res, next) => {
  res.status(200).json({ message: "ok" });
  // const id = req.params.ID;
  // User.deleteOne({ _id: id })
  //   .exec()
  //   .then((result) => {
  //     console.log(result);
  //     res.status(200).json({
  //       message: "User deleted",
  //       request: {
  //         type: "DELETE",
  //         protected: true,
  //         url: serverUrl + id
  //       }
  //     });
  //   })
  //   .catch((err) => {
  //     console.log(err);
  //     res.status(500).json({
  //       error: err,
  //       request: {
  //         type: "DELETE",
  //         params: [{ userID: "Number" }]
  //       }
  //     });
  //   });
};
exports.users_update = (req, res, next) => {
  const id = req.params.userID;
  const updateOps = {};
  if (req.body.length) {
    for (const ops of req.body) {
      if (ops.propName != "role" || "owner")
        updateOps[ops.propName] = ops.value;
      else console.log("User role is not a modifiable field");
    }
    User.update({ _id: id }, { $set: updateOps })
      .exec()
      .then((result) => {
        res.status(200).json({
          message: "User updated",
          user: result,
          request: {
            type: "PATCH",
            url: serverUrl + id,
            protected: true
          }
        });
      })
      .catch((err) => {
        console.log(err);
        res.status(500).json({
          error: err + "string",
          request: {
            type: "PATCH",
            url: serverUrl + id,
            protected: true,
            description:
              "Updates user by ID. Accepts user ID as URL param, and an array as parameters you wish to modify",
            bodyParams: [{ propName: "email" }, { value: "your_value" }]
          }
        });
      });
  } else {
    res.status(500).json({
      message: "Endpoint accepts only arrays as body parameters",
      request: {
        type: "PATCH",
        url: serverUrl + id,
        protected: true,
        description:
          "Updates user by ID. Accepts user ID as URL param, and an array as parameters you wish to modify",
        bodyParams: [
          { propName: "your_property_name" },
          { value: "your_value" }
        ],
        viableProperties: "email:String, password:String"
      }
    });
  }
};

exports.users_get_one_by_ID = (req, res, next) => {
  const id = req.params.userID;
  User.findById(id)
    .select("-password -__v -role")
    .exec()
    .then((result) => {
      if (result) {
        res.status(200).json({
          user: result,
          request: {
            type: "GET",
            description: "Fetches user by ID specified in URL params",
            urlParams: ["/id"]
          }
        });
      } else {
        res.status(404).json({ message: "No valid Entry for provided ID" });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};
exports.users_create = (req, res, next) => {
  // const role = req.body.role;
  // if (!(role == "vendor" || role == "customer")) {
  //   res.status(500).json({ error: { message: "invalid role" } });
  // }
  // User.find({ email: req.body.email })
  //   .exec()
  //   .then((user) => {
  //     if (user.length >= 1) {
  //       return res.status(409).json({
  //         message: "Email already in use  "
  //       });
  //     } else {
  //       bcrypt.hash(req.body.password, 10, (err, hash) => {
  //         if (err) {
  //           return res.status(500).json({ error: err });
  //         } else {
  //           const user = new User({
  //             _id: new mongoose.Types.ObjectId(),
  //             email: req.body.email,
  //             password: hash,
  //             role: req.body.role
  //           });
  //           user.owner = user._id;
  //           user
  //             .save()
  //             .then((result) => {
  //               res.status(200).json({
  //                 message: "User Created",
  //                 user: { userID: result._id, email: result.email }
  //               });
  //             })
  //             .catch((err) => {
  //               console.log(err);
  //               res.status(500).json({ error: err });
  //             });
  //         }
  //       });
  //     }
  //   });
};
