const Product = require("../models/product");
const mongoose = require("mongoose");
const serverUrl = process.env.SERVER_URL + "products/";
// const User = require("../models/user");

exports.products_get_all = (req, res, next) => {
  console.log("host:", req.hostname);
  console.log("host2", req.get("host"));
  Product.find()
    .select("-__v")
    // .populate("vendor")
    .exec()
    .then((docs) => {
      const response = {
        request: {
          type: "GET",
          description: "Get all Products",
          url: serverUrl
        },
        count: docs.length,
        products: docs
      };

      if (docs.length > 0) {
        res.status(200).json(response);
      } else {
        res.status(200).json({ message: "No entries found" });
      }
    })
    .catch((err) => {
      console.log(err);
      res.send(500).json({ error: err });
    });
};

exports.products_create = (req, res, next) => {
  // console.log("body: ", JSON.parse(req.body.product), "\n ");
  let productObject = JSON.parse(req.body.product);

  const product = new Product({
    _id: new mongoose.Types.ObjectId(),
    name: productObject.name,
    price: productObject.price,
    sizes: productObject.sizes,
    ingredients: productObject.ingredients,
    type: productObject.type,
    productImage: req.file.path,
    productImageAbsolute: "https://" + req.get("host") + "/api/" + req.file.path
  });
  // console.log(product);
  product
    .save()
    .then((result) => {
      console.log("CREATED PRODUCT: ", result);
      res.status(201).json({
        message: "Created Product successfully",
        createdProduct: {
          name: result.name,
          price: result.price,
          _id: result._id
        },
        request: {
          type: "PUT",
          url: serverUrl + result._id,
          protected: true
        }
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};
exports.products_get_one_by_id = (req, res, next) => {
  const id = req.params.ID;
  Product.findById(id)
    // .populate("vendor", "_id email")
    .exec()
    .then((result) => {
      console.log(result);
      if (result) {
        res.status(200).json({
          name: result.name,
          price: result.price,
          _id: result._id,
          sizes: result.sizes,
          ingredients: result.ingredients,
          productImage: result.productImage,
          productImageAbsolute: result.productImageAbsolute,
          owner: result.owner,
          type: result.type,
          added: result.added,
          request: {
            type: "GET",
            url: serverUrl + result._id
          }
        });
      } else {
        res.status(404).json({ message: "No valid Entry for provided ID" });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};
exports.products_update = (req, res, next) => {
  const id = req.params.ID;
  let productProperties;
  try {
    productProperties = JSON.parse(req.body.product);
  } catch (err) {
    res.status(500).json({ err: err });
  }
  if (productProperties && productProperties.length) {
    // console.log(productProperties);
    const updateOps = {};
    for (const ops of productProperties) {
      updateOps[ops.propName] = ops.value;
    }
    if (req.file && req.file.path) {
      updateOps["productImage"] = req.file.path;
      updateOps["productImageAbsolute"] =
        "https://" + req.get("host") + "/api/" + req.file.path;
    }
    Product.update({ _id: id }, { $set: updateOps })
      .exec()
      .then((result) => {
        console.log(result);
        res.status(200).json({
          message: "Product updated",
          request: {
            type: "PATCH",
            url: serverUrl + id
          }
        });
      })
      .catch((err) => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  } else {
    res.status(500).json({
      message: "Endpoint accepts only arrays as body parameters",
      request: {
        type: "PATCH",
        url: serverUrl + id,
        description:
          "Updates product by ID. Accepts product ID as URL param, and an array as parameters you wish to modify",
        bodyParams: [
          { propName: "your_property_name" },
          { value: "your_value" }
        ],
        viableProperties: "name:String, price:number"
      }
    });
  }
};
exports.products_delete = (req, res, next) => {
  const id = req.params.ID;
  Product.deleteOne({ _id: id })
    .exec()
    .then((result) => {
      console.log(result);
      res.status(200).json({
        message: "Product deleted",
        request: {
          type: "DELETE"
        }
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
        request: {
          type: "DELETE",
          params: [{ ID: "Number" }]
        }
      });
    });
};
