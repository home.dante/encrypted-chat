const MongoClient = require("mongodb").MongoClient;
const client = require("socket.io").listen(4000).sockets;
const path = require("path");


const assert = require('assert');

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = "encryptedChat";

// Create a new MongoClient
const mongoClient = new MongoClient(url);
const express = require("express");
const app = express();
//Port number
const port = process.env.PORT || 8080;

//Cors MiddleWare
// app.use(cors());

//Set Static Folder File
app.use(express.static(path.join(__dirname, "public")));

app.get("/", (req, res) => {
  console.log("nope");
  res.sendFile(path.join(__dirname, "public/index.html"));
});

//Start Server
app.listen(port, () => {
  console.log("Server started on port " + port);
});


// Use connect method to connect to the Server
mongoClient.connect(function(err) {
  assert.equal(null, err);
  console.log("Connected successfully to server");

  const db = mongoClient.db(dbName);
  let chat = db.collection("chats");
  connectSocket(chat);
});
  // client.close();

function connectSocket(chat) {
  client.on("connection", function(socket) {
    // Create function to send status
    sendStatus = function(s) {
      socket.emit("status", s);
    };

    // get chats from database
    chat
      .find()
      .limit(100)
      .sort({ _id: 1 })
      .toArray(function(err, res) {
        if (err) {
          throw err;
        }

        //emit messages
        socket.emit("output", res);
      });

    //handle input
    socket.on("input", function(data) {
      console.log("input came ", data)
      let name = data.name;
      let message = data.message;

      //check for name and message
      if (name == "" || message == "") {
        sendStatus("Please enter a name and a message");
      } else {
        //insert message
        chat.insert({ name: name, message: message }, function() {
          client.emit("output", [data]);

          //send status object
          sendStatus({
            message: "Message sent",
            clear: "true"
          });
        });
      }
    });

    //handle clear
    socket.on("clear", function(data) {
      //remove all chats from collection
      chat.remove({}, function() {
        //emit clear
        socket.emit("cleared");
        sendStatus({
          message: "Cleared",
          clear: "true"
        });
      });
    });
  });
}
